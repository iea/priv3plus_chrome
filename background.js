// XXX: keep the trigger domains of each tab as well as other summary data
// this will be used for the info to show to the user
// any update will happen through chrome.extension.sendMessage from
// the content script in the tab (i.e., tabSummaryMap)
// 1. location URL
// 2. first-party site
// 3. third parties {cookiesRemoved, userInteraction, saved request URLs}
// 4. loadTime
// 5. hostnameToDomainMap
var tabInfoMap = {};

// XXX: ports we'll use to communicate with main frames of tabs
var portMainFrame = {};

// XXX: each content script will receive some read-only values (i.e., receivedTabData)
// 1. location URL
// 2. first-party site
// 3. third parties {cookiesRemoved}
// 4. hostnameToDomainMap for not calling the getDomain function from the suffix list
// 5. highlight options
// 6. exceptions
// XXX: keep one such data structure for all content scripts in all frame of one tab
var sentTabMap = {};


// XXX: each tab will also keep its own data (i.e., localTabData)
// 1. third parties {savedElems}
// 2. highlightList
// 3. highlightThirdPartyList for hovering mouse info
// 4. reloaded items

// XXX: prefs
var prefShouldHighlight;
var prefHighlightOptionsList;
var prefDisabledSitesList;
var prefDisabledPagesList;
var prefAllowedExceptionsList;

// XXX: returns a new summary data structure for the background page
function getNewTabInfo()
{
	var tab = {};
	
	tab["loc"] = null;
	tab["site"] = null;
	
	// XXX: reset the saved third parties for this tab
	tab["thirdParties"] = {};
	
	tab["loadTime"] = 0;
	
	// XXX: used for reverse mapping elements to domains for highlighting
	// XXX: so that we don't need to determine third party domain again
	tab["hostnameToDomainMap"] = {};
	
	tab["reloaded"] = {};
	
	tab["excepted"] = {};

	tab["cachedAllowedExceptionHosts"] = [];
	
	// XXX: shortcut for total count
	tab["totalThirdParties"] = 0;

	return tab;
}

// XXX: returns a new tab data structure to be sent to the content script
// just keep one and share it with all content scripts 
function getNewSentTab(tabInfo)
{
	var tab = {};
	
	tab["location"] = tabInfo["loc"];
	tab["site"] = tabInfo["site"];
	
	tab["thirdParties"] = {};
	var thirdParties = tabInfo["thirdParties"];
	for (var tp in thirdParties)
	{
		tab["thirdParties"][tp] = {};
		tab["thirdParties"][tp].cookiesRemoved = thirdParties[tp].cookiesRemoved;
	}

	// XXX: used for reverse mapping elements to domains for highlighting
	// XXX: so that we don't need to determine third party domain again
	tab["hostnameToDomainMap"] = tabInfo["hostnameToDomainMap"];
	
	// XXX: highlight preferences
	tab["highlight"] = prefShouldHighlight;
	tab["highlightOptionsList"] = prefHighlightOptionsList;

	tab["cachedAllowedExceptionHosts"] = tabInfo["cachedAllowedExceptionHosts"];

	if (tabInfo["disabledSite"] || tabInfo["disabledPage"])
	{
		tab["disabled"] = true;
	}
	
	//console.log(tab);

	return tab;
}

// XXX: TLD applications using public suffix list
function updatePublicSuffixList()
{
	var ts = localStorage.getItem("timestamp_psl");
	var date = new Date();
	var now = date.getTime() / 1000.0;
	if (ts && now - ts < 86400)
	{
		//console.log("getting from storage");
		var list = localStorage.getItem("psl");
		window.publicSuffixList.parse(list, punycode.toASCII);
	}
	else
	{
		//console.log("getting a new version of the list");
		var request = new XMLHttpRequest();
		request.open('GET', "https://publicsuffix.org/list/effective_tld_names.dat", true);
		request.onreadystatechange = function(anEvent)
		{
			if(request.readyState == 4 && request.status == 200)
			{
				var list = request.responseText;
				//console.log(list);
				window.publicSuffixList.parse(list, punycode.toASCII);

				// XXX: store the list
				localStorage.setItem("psl", list);
				var date = new Date();
				var now = date.getTime() / 1000.0;
				localStorage.setItem("timestamp_psl", now);
			}
		};
		request.send(null);
	}
}

function getHostname(url) 
{
	var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/]+)', 'im');
	var match = url.match(re);
	if(match)
	{
		return match[1].toString();
	}
	return null;
}

function getDomain(url)
{
	return window.publicSuffixList.getDomain(getHostname(url));
}

function updateHighlights(shouldHighlight, highlightOptionsList)
{
    // XXX: call content script to update highlights
	for (var tabId in tabInfoMap)
	{
		tabId = parseInt(tabId);
		//console.log("updating highlight: " + tabId);
		if (!portMainFrame[tabId])
		{
			portMainFrame[tabId] = chrome.tabs.connect(tabId, {name: "main_frame_" + tabId});
		}
		portMainFrame[tabId].postMessage({msg: "updateHighlight", sh: shouldHighlight, hol: highlightOptionsList});
	}

}

function toggleHighlight()
{
    //console.log("toggling highlight... " + prefShouldHighlight);
	prefShouldHighlight = !prefShouldHighlight;
	localStorage.setItem("prefShouldHighlight", JSON.stringify(prefShouldHighlight));    
    //console.log("done. " + prefShouldHighlight);

    updateHighlights(prefShouldHighlight, prefHighlightOptionsList);
}

function setHighlightOptions(shouldHighlight, highlightOptionsList)
{
	prefShouldHighlight = shouldHighlight;
	localStorage.setItem("prefShouldHighlight", JSON.stringify(shouldHighlight));
	
    prefHighlightOptionsList = highlightOptionsList;
    localStorage.setItem("prefHighlightOptionsList", JSON.stringify(highlightOptionsList));
    
    updateHighlights(shouldHighlight, highlightOptionsList);
}
function getHighlightOptions()
{
	return [prefShouldHighlight, prefHighlightOptionsList];
}

// XXX: called from the popup to show info
function getTabInfo(tabId)
{
	return tabInfoMap[tabId];
}

function isDisabledSite(site)
{
	if (prefDisabledSitesList.indexOf(site) == -1)
	{
		return false;
	}
	return true;
}

function isDisabledPage(url)
{
	var index = url.indexOf("#");
	if (index != -1)
	{
		url = url.substring(0, index);
	}

	if (prefDisabledPagesList.indexOf(url) == -1)
	{
		return false;
	}
	return true;
}

function addDisabledSite(site)
{
	if (site == null)
	{
		return;
	}
	if (prefDisabledSitesList.indexOf(site) == -1)
	{
		prefDisabledSitesList.push(site);
		localStorage.setItem("prefDisabledSitesList", JSON.stringify(prefDisabledSitesList));
	}
}

function addDisabledPage(url)
{
	if (url.indexOf("about:") == 0)
	{
		return;
	}

	var index = url.indexOf("#");
	if (index != -1)
	{
		url = url.substring(0, index);
	}

	if (prefDisabledPagesList.indexOf(url) == -1)
	{
		prefDisabledPagesList.push(url);
		localStorage.setItem("prefDisabledPagesList", JSON.stringify(prefDisabledPagesList));
	}
}

function removeDisabledSite(site)
{
	var index = prefDisabledSitesList.indexOf(site);
	if (index != -1)
	{
		prefDisabledSitesList.splice(index, 1);
		localStorage.setItem("prefDisabledSitesList", JSON.stringify(prefDisabledSitesList));
	}
}

function getDisabledSites()
{
    return prefDisabledSitesList;
}

function getDisabledPages()
{
    return prefDisabledPagesList;
}

function removeDisabledPage(url)
{
	var index = url.indexOf("#");
	if (index != -1)
	{
		url = url.substring(0, index);
	}

	var index2 = prefDisabledPagesList.indexOf(url);
	if (index2 != -1)
	{
		prefDisabledPagesList.splice(index2, 1);
		localStorage.setItem("prefDisabledPagesList", JSON.stringify(prefDisabledPagesList));
	}
}

function removeAllDisabledSites()
{
    prefDisabledSitesList = [];
	localStorage.setItem("prefDisabledSitesList", JSON.stringify(prefDisabledSitesList));
}

function removeAllDisabledPages()
{
    prefDisabledPagesList = [];
	localStorage.setItem("prefDisabledPagesList", JSON.stringify(prefDisabledPagesList));
}

function isCachedAllowedException(thirdPartyDomain, tab)
{
	if (tab.cachedAllowedExceptionHosts.indexOf(thirdPartyDomain) != -1)
	{
		return true;
	}
	return false;
}

function checkAllowedExceptions(thirdPartyDomain, triggerDomain, tab)
{
	// XXX: shortcut for already-checked third party domains
	if (isCachedAllowedException(thirdPartyDomain, tab))
	{
		//console.log("cached allowed: " + thirdPartyDomain + " on: " + triggerDomain);
		return true;
	}

	//console.log("checking third party: " + thirdPartyDomain + " on: " + triggerDomain);
	var sites = prefAllowedExceptionsList[thirdPartyDomain];
	if (!sites)
	{
		// XXX: no entry for third party domain: not allowed exception
		return false;
	}

	for (var s in sites)
	{
		var site = sites[s];
		//console.log("site: " + site);
		if (site == "*" || site == triggerDomain)
		{
			console.log("allowed: " + thirdPartyDomain + " on: " + triggerDomain);
			tab.cachedAllowedExceptionHosts.push(thirdPartyDomain);
			return true;
		}
	}
	return false;
}

function getAllowedExceptions()
{
	return prefAllowedExceptionsList;
}

function addException(thirdparty, site)
{
//	console.log("going to add new exception: " + thirdparty + " " + site);
	if (!prefAllowedExceptionsList[thirdparty])
	{
		prefAllowedExceptionsList[thirdparty] = [];
	}
	
	if (prefAllowedExceptionsList[thirdparty][0] == "*")
	{
		return;
	}
	else
	{
		if (site == "*")
		{
			prefAllowedExceptionsList[thirdparty] = [];
		}
		if (prefAllowedExceptionsList[thirdparty].indexOf(site) == -1)
		{
			prefAllowedExceptionsList[thirdparty].push(site);
		}
	}

	localStorage.setItem("prefAllowedExceptionsList", JSON.stringify(prefAllowedExceptionsList));
}

function removeAllExceptions()
{
//	console.log("going to remove all exceptions...");
	prefAllowedExceptionsList = {};
	localStorage.setItem("prefAllowedExceptionsList", JSON.stringify(prefAllowedExceptionsList));
}

function removeException(thirdparty, site)
{
//	console.log("going to remove exception: " + thirdparty + " " + site);
	var newsites = prefAllowedExceptionsList[thirdparty];
	var idx = newsites.indexOf(site);
	if (idx != -1)
	{
		newsites.splice(idx, 1);
	}
	prefAllowedExceptionsList[thirdparty] = newsites;
	
	localStorage.setItem("prefAllowedExceptionsList", JSON.stringify(prefAllowedExceptionsList));	
}


chrome.webRequest.onBeforeSendHeaders.addListener(
	function (request) 
	{ 
		// TODO
		// 1. check request cookies
		// 2. if there are cookies, check 3rd party domain and remove if necessary
		// 3. save elements and cookies that were going to be loaded with cookies
		// 4. capture user interaction and reload element with cookies
		// 4.1 distinguish social widgets and advertisements
		// 5. add info for the user (in the popup)
		// 6. add options for the user (in the popup)
		// 6.1 add highlight options
		// 6.2 add exceptions options
		
		//console.log(request);

		
		// XXX: this is the first-party site
		var triggerDomain = null;
		
		var requestHostname = getHostname(request.url);
		
		var tabId = request.tabId;

		// XXX: learn the trigger domain in the tab via this way
		// XXX: chrome.tabs.get(request.tabId) is asynchronous; thus, getting the tab URL and 
		// then modifying http headers becomes trickier
		if (request.type == "main_frame" && request.parentFrameId == -1)
		{
			//console.log(request.url);
			//console.log(request.frameId);
			var tabInfo = getNewTabInfo();
			tabInfo["loc"] = request.url;
			tabInfo["site"] = getDomain(request.url);
			
			// XXX: record the request URL and the domain to be used while highlighting
			tabInfo["hostnameToDomainMap"][requestHostname] = tabInfo["site"];
			
			tabInfoMap[tabId] = tabInfo;
			portMainFrame[tabId] = null;
			
			//console.log(tabInfoMap[tabId]);
		}
		
		var tabInfo = tabInfoMap[tabId];
		if (tabInfo && tabInfo["site"])
		{
			triggerDomain = tabInfo["site"];
		}
		
		// XXX: check whole-site exceptions (e.g., allow * on example.com)
		if (isDisabledSite(triggerDomain))
		{
			console.log("disabled on site: " + triggerDomain);
			tabInfo["disabledSite"] = true;
			return {requestHeaders: request.requestHeaders};
		}
		
		// XXX: check page exceptions (e.g., allow * on example.com/index.html)
		if (tabInfo && isDisabledPage(tabInfo["loc"]))
		{
			console.log("disabled on page: " + tabInfo["loc"]);
			tabInfo["disabledPage"] = true;
			return {requestHeaders: request.requestHeaders};			
		}

		var requestDomain = getDomain(request.url);
		
		// XXX: assume we are going to remove the cookies
		var shouldRemoveCookies = true;
		
		if (triggerDomain && triggerDomain == requestDomain)
		{
			shouldRemoveCookies = false;
			// XXX: record the request URL and the domain to be used while highlighting
			tabInfoMap[tabId]["hostnameToDomainMap"][requestHostname] = tabInfo["site"];
		}
		
		// XXX: check for excepted trackers on specific sites
		if (tabInfo && checkAllowedExceptions(requestDomain, triggerDomain, tabInfo))
		{
			shouldRemoveCookies = false;
			if (!tabInfo.excepted[requestDomain])
			{
				tabInfo.excepted[requestDomain] = [];
			}
			tabInfo.excepted[requestDomain].push(request.url);
			tabInfoMap[tabId]["hostnameToDomainMap"][requestHostname] = requestDomain;
		}
		
		if (tabId != -1 && triggerDomain && shouldRemoveCookies)
		{
			//console.log("trigger domain: " + triggerDomain + " request domain: " + requestDomain);
			/*
			//if (request.type.indexOf("frame") != -1)
			{
				//console.log(request.type + " " + request.frameId + " " + request.parentFrameId);
				console.log(request.url);
			}
			*/

			// XXX: initialize the third party for this tab
			if (tabId != -1 && !tabInfo["thirdParties"][requestDomain])
			{
				var thirdParty = {};
				thirdParty.userInteraction = false;
				thirdParty.cookiesRemoved = false;
				thirdParty.savedElemURLs = [];
				
				tabInfo["thirdParties"][requestDomain] = thirdParty;
				// XXX: update the popup count
				tabInfo["totalThirdParties"]++;
				try
				{
                    // XXX: update the popup icon
                    chrome.browserAction.setBadgeBackgroundColor({"color": [80, 80, 200, 100]});
                    chrome.browserAction.setBadgeText({"text": "" + tabInfo["totalThirdParties"], "tabId": tabId});
				}
				catch (e)
				{
				}
			}
			
			if (!tabInfo["thirdParties"][requestDomain].userInteraction)
			{
				// XXX: remove the cookies from the third party requests
				var len = request.requestHeaders.length;
				for (var i = 0; i < len; i++)
				{
					if (request.requestHeaders[i].name === "Cookie")
					{
						request.requestHeaders.splice(i, 1);
						//console.log("cookie removed: " + request.url);
						tabInfo["thirdParties"][requestDomain].cookiesRemoved = true;
						break;
					}
				}

				// XXX: save the third party element URL, so that we can show info to the user 
				// and then pick the reloadable ones when the user interacts with them
				tabInfo["thirdParties"][requestDomain].savedElemURLs.push(request.url);
				// XXX: record the request URL and the domain to be used while highlighting
				tabInfo["hostnameToDomainMap"][requestHostname] = requestDomain;
				
			}
			else
			{
				//console.log("user must have clicked something: " + requestDomain);
				//console.log(request.url);
			}
		}
		else if (!triggerDomain)
		{
			// FIXME: weird exception when there is a request without a proper tabId
			//console.log("undefined tab: " + request.tabId + " " + requestDomain);
			// XXX: just remove cookies
			var len = request.requestHeaders.length;
			for (var i = 0; i < len; i++)
			{
				if (request.requestHeaders[i].name === "Cookie")
				{
					request.requestHeaders.splice(i, 1);
					//console.log("cookie removed: " + request.url);
					break;
				}
			}
		}
		
		return {requestHeaders: request.requestHeaders};
	},
	{urls: ["<all_urls>"]},
	["blocking", "requestHeaders"]);

/*
chrome.webRequest.onSendHeaders.addListener(
        function (request) { 
        	return;
            //console.log(request);
            var hasCookies = hasCookies(request);
            if (hasCookies)
            {
                console.log("has cookies: " + request.url);
            }
            else
            {
                console.log("no cookies: " + request.url);
                
            }
            //console.log("===========");
        },
        {urls: ["<all_urls>"]},
        ["requestHeaders"]);
*/

chrome.extension.onMessage.addListener(
	function(data, sender, callback) 
	{
		if (!sender.tab)
		{
			return;
		}
		
		var tabId = sender.tab.id;
		
		if (data.msg == "loadTime")
		{
			//console.log(tabId);
			
			tabInfoMap[tabId]["loadTime"] = data.lt;

			// XXX: init shared tab data
			var sentTab = getNewSentTab(tabInfoMap[tabId]);
			//console.log(sentTab);
			
			callback(sentTab);
		}
		else if (data.msg == "reload")
		{
			//console.log(data);
			if (!portMainFrame[tabId])
			{
				portMainFrame[tabId] = chrome.tabs.connect(tabId, {name: "main_frame_" + tabId});
			}
			portMainFrame[tabId].postMessage({msg: "reload", url: data.url, type: data.type});
		}
		else if (data.msg == "click")
		{
			tabInfoMap[tabId]["thirdParties"][data.tp].userInteraction = true;
		}
		else if (data.msg == "reloadedItems")
		{
			//console.log(data);
			tabInfoMap[tabId]["reloaded"][data.tp] = data.ri;
		}
		else if (data.msg == "iframeMain")
		{
			var portIframe = chrome.tabs.connect(tabId, {name: "iframe_" + data.src});
			//console.log("iframeMain reload request");
			//console.log(portIframe);
			portIframe.postMessage({iframeMain: true, url: data.src});
		}
		else
		{
			console.log(data);
		}
	});


function initPrefs()
{
	prefShouldHighlight = JSON.parse(localStorage.getItem("prefShouldHighlight"));
	if (!prefShouldHighlight)
	{
		prefShouldHighlight = false;
		localStorage.setItem("prefShouldHighlight", JSON.stringify(prefShouldHighlight));
	}
	
	prefHighlightOptionsList = localStorage.getItem("prefHighlightOptionsList");
	if (!prefHighlightOptionsList)
	{
		prefHighlightOptionsList = {};
		prefHighlightOptionsList["main"] = true;
		prefHighlightOptionsList["iframe"] = true;
		prefHighlightOptionsList["other"] = true;
		prefHighlightOptionsList["clicked"] = true;
		prefHighlightOptionsList["reloaded"] = true;
		prefHighlightOptionsList["excepted"] = true;
		
		localStorage.setItem("prefHighlightOptionsList", JSON.stringify(prefHighlightOptionsList));
	}
	else
	{
		prefHighlightOptionsList = JSON.parse(prefHighlightOptionsList);
	}
	
	prefDisabledSitesList = JSON.parse(localStorage.getItem("prefDisabledSitesList"));
	if (!prefDisabledSitesList)
	{
		prefDisabledSitesList = [];
		
		localStorage.setItem("prefDisabledSitesList", JSON.stringify(prefDisabledSitesList));
	}
	prefDisabledPagesList = JSON.parse(localStorage.getItem("prefDisabledPagesList"));
	if (!prefDisabledPagesList)
	{
		prefDisabledPagesList = [];
	
		localStorage.setItem("prefDisabledPagesList", JSON.stringify(prefDisabledPagesList));
	}

	prefAllowedExceptionsList = JSON.parse(localStorage.getItem("prefAllowedExceptionsList"));
	if (!prefAllowedExceptionsList)
	{
	    prefAllowedExceptionsList = {};
	    
		localStorage.setItem("prefAllowedExceptionsList", JSON.stringify(prefAllowedExceptionsList));
	}

}

function initPriv3Plus()
{
	initPrefs();
}

initPriv3Plus();

updatePublicSuffixList();

