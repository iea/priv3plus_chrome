var backgroundPage = chrome.extension.getBackgroundPage();

function toggleHighlight(event)
{
   	var checkbox = event.target;
	var checked = checkbox.getAttribute("checked");
	if (checked == "checked")
	{
		checkbox.removeAttribute("checked");
	}
	else
	{
		checkbox.setAttribute("checked", "checked");
	}
	
	var id = checkbox.getAttribute("id");
	if (id == "shouldHighlight")
	{
		backgroundPage.toggleHighlight();
	}

}

function getCheckboxElement(id, checked)
{
	var checkbox = document.createElement("input");
	checkbox.setAttribute("type", "checkbox");
	checkbox.setAttribute("id", id);
	checkbox.setAttribute("name", id);
	checkbox.setAttribute("value", id);
	
	if (checked)
	{
		checkbox.setAttribute("checked", "checked");
	}
		
	checkbox.addEventListener('click', function(event) { toggleHighlight(event); } );
	
	return checkbox;
}

function populateOptionsList(shouldHighlight)
{
   	var divOptions = document.getElementById("options");

	while (divOptions.firstChild)
	{
		divOptions.removeChild(divOptions.firstChild);
	}

	// XXX: always enable
	var trShouldHighlight = getCheckboxElement("shouldHighlight", shouldHighlight);
	var label = document.createElement("label");
	var text2 = document.createTextNode("Highlight third party content");
	label.appendChild(text2);
	
	divOptions.appendChild(trShouldHighlight);
	divOptions.appendChild(label);

	var br = document.createElement("p");
	divOptions.appendChild(br);
	
	var le = document.createElement("a");
	le.innerText = "Open highlight options";
	le.href = "#";
	le.title = "Open highlight options";
	le.addEventListener("click", function () { chrome.tabs.create({url: "highlight_options.html", active:true}); }, false);
	
	divOptions.appendChild(le);
	
	var br = document.createElement("p");
	divOptions.appendChild(br);

	var le = document.createElement("a");
	le.innerText = "Open exception options";
	le.href = "#";
	le.title = "Open exception options";
	le.addEventListener("click", function () { chrome.tabs.create({url: "exception_options.html", active:true}); }, false);
	
	divOptions.appendChild(le);
	
	var br = document.createElement("p");
	divOptions.appendChild(br);

	var lo = document.createElement("a");
	lo.innerText = "About Priv3+";
	lo.href = "#";
	lo.title = "About Priv3+";
	lo.addEventListener("click", function () { chrome.tabs.create({url: "about.html", active:true}); }, false);
	
	divOptions.appendChild(lo);
	
	var hr2 = document.createElement("hr");
	divOptions.appendChild(hr2);

}

function getTableRow(t, len, isHeader)
{
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	if (isHeader)
	{
		var b = document.createElement("b");
		b.innerText = t;
		td.appendChild(b);
	}
	else
	{
		td.innerText = t;
	}
	
	var td2 = document.createElement("td");
	td2.setAttribute("align", "center");
	if (isHeader)
	{
		var b2 = document.createElement("b");
		b2.innerText = len;
		td2.appendChild(b2);
	}
	else
	{
		td2.innerText = len;
	}
	
	tr.appendChild(td);
	tr.appendChild(td2);

	return tr;
}

function getListElement(value)
{
	var li = document.createElement("li");
	var label = document.createElement("label");
	var text = document.createTextNode(value);
	label.appendChild(text);
	
	li.appendChild(label);
	return li;
}

function getInfoElement(title, value)
{
	var p = document.createElement("div");
	var b = document.createElement("b");
	b.innerText = title;
	var t = document.createTextNode(value);
	
	p.appendChild(b);
	p.appendChild(t);
	
	return p;
}


function displayTabInfo(tabInfo)
{
	// XXX: display all tab info
	// 1. site/location info
	// 2. page load time
	// 3. filtered third party list
	// 4. reloaded third party list
	// 5. excepted third party list
	
	var hInfo = getInfoElement("Information", "");
	var site = getInfoElement("Site: ", tabInfo["site"]);
	var lds;
	if (!tabInfo["disabledSite"])
	{
		lds = document.createElement("a");
		lds.innerText = "Disable Priv3+ on this site?";
		lds.title = "Click to disable Priv3+ on this site";
		lds.href = "#";
		lds.addEventListener("click", function() { backgroundPage.addDisabledSite(tabInfo["site"]); tabInfo["disabledSite"] = true; displayTabInfo(tabInfo);}, false);
	}
	else
	{
		lds = document.createElement("a");
		lds.innerText = "Enable Priv3+ on this site?";
		lds.title = "Click to enable Priv3+ on this site";
		lds.href = "#";
		lds.addEventListener("click", function() { backgroundPage.removeDisabledSite(tabInfo["site"]); tabInfo["disabledSite"] = false; displayTabInfo(tabInfo);}, false);		
	}
	var loc = getInfoElement("Page: ", tabInfo["loc"]);
	var ldp;
	
	if (!tabInfo["disabledPage"])
	{
		ldp = document.createElement("a");
		ldp.innerText = "Disable Priv3+ on this page?";
		ldp.title = "Click to disable Priv3+ on this page";
		ldp.href = "#";
		ldp.addEventListener("click", function() { backgroundPage.addDisabledPage(tabInfo["loc"]); tabInfo["disabledPage"] = true; displayTabInfo(tabInfo);}, false);
	}
	else
	{
		ldp = document.createElement("a");
		ldp.innerText = "Enable Priv3+ on this page?";
		ldp.title = "Click to enable Priv3+ on this page";
		ldp.href = "#";
		ldp.addEventListener("click", function() { backgroundPage.removeDisabledPage(tabInfo["loc"]); tabInfo["disabledPage"] = false; displayTabInfo(tabInfo);}, false);
	}

	var loadtime = getInfoElement("Pageload: ", tabInfo["loadTime"] + " ms");
	var counttp = tabInfo["totalThirdParties"] + " domain";
	if (tabInfo["totalThirdParties"] == 0 || tabInfo["totalThirdParties"] > 1)
	{
		counttp += "s";
	}
	var totaltp = getInfoElement("# third parties: ", counttp);
	
	var hFiltered = getInfoElement("Filtered", "");
	var tFiltered = document.createElement("table");
	tFiltered.setAttribute("border", "1");
	var tHeader = getTableRow("Third party", "# requests", true);
	tFiltered.appendChild(tHeader);
	
	var hNoCookies = getInfoElement("No cookies", "");
	var tNoCookies = document.createElement("table");
	tNoCookies.setAttribute("border", "1");
	var tHeader = getTableRow("Third party", "# requests", true);
	tNoCookies.appendChild(tHeader);

	var countFiltered = 0;
	var countFilteredReq = 0;
	var countNoCookies = 0;
	var countNoCookiesReq = 0;
	var filteredItems = [];
	var noCookiesItems = [];
	for (var tp in tabInfo["thirdParties"])
	{
		if (tabInfo["thirdParties"][tp].cookiesRemoved)
		{
            countFiltered++;
            var len = tabInfo["thirdParties"][tp].savedElemURLs.length;
            countFilteredReq += len;
            filteredItems.push([tp, len]);
		}
		else
		{
		    countNoCookies++;
            var len = tabInfo["thirdParties"][tp].savedElemURLs.length;
            countNoCookiesReq += len;
            noCookiesItems.push([tp, len]);
		}
	}
	
	filteredItems.sort(function(a, b)
		{
			return a[0].localeCompare(b[0]);
		});
	for (var i = 0; i < filteredItems.length; i++)
	{
		var tp = filteredItems[i][0];
		var len = filteredItems[i][1];
		var litp = getTableRow(tp, len, false);
		tFiltered.appendChild(litp);
	}
	
    noCookiesItems.sort(function(a, b)
		{
			return a[0].localeCompare(b[0]);
		});
	for (var i = 0; i < noCookiesItems.length; i++)
	{
		var tp = noCookiesItems[i][0];
		var len = noCookiesItems[i][1];
		var litp = getTableRow(tp, len, false);
		tNoCookies.appendChild(litp);
	}

	var f = countFiltered + " domain";
	if (countFiltered > 1)
	{
		f += "s";
	}
	f += ", " + countFilteredReq + " request";
	if (countFilteredReq > 1)
	{
		f += "s";
	}
	var filtered = getInfoElement("Filtered: ", f);
	
	var nc = countNoCookies + " domain";
	if (countNoCookies > 1)
	{
		nc += "s";
	}
	nc += ", " + countNoCookiesReq + " request";
	if (countNoCookiesReq > 1)
	{
		nc += "s";
	}
	var nocookies = getInfoElement("No cookies: ", nc);
	
	var hReloaded = getInfoElement("Reloaded", "");
	var tReloaded = document.createElement("table");
	tReloaded.setAttribute("border", "1");
	var tHeader2 = getTableRow("Third party", "# requests", true);
	tReloaded.appendChild(tHeader2);

	var countReloaded = 0;
	var countReloadedReq = 0;
	var reloadedItems = [];
	for (var tp in tabInfo["reloaded"])
	{
		countReloaded++;
		var value = " (";
		for (var t in tabInfo["reloaded"][tp])
		{
			var len = tabInfo["reloaded"][tp][t].length;
			countReloadedReq += len;
			value += t + ": " + len + ", ";
		}
		value = value.substring(0, value.length -2) + ")";
		reloadedItems.push([tp, value]);
	}
	
	reloadedItems.sort(function(a, b)
		{
			return a[0].localeCompare(b[0]);
		});
	for (var i = 0; i < reloadedItems.length; i++)
	{
		var tp = reloadedItems[i][0];
		var value = reloadedItems[i][1];
		var litp = getTableRow(tp, value, false);
		tReloaded.appendChild(litp);		
	}
	
	var r = countReloaded + " domain";
	if (countReloaded > 1)
	{
		r += "s";
	}
	r += ", " + countReloadedReq + " request";
	if (countReloadedReq > 1)
	{
		r += "s";
	}
	var reloaded = getInfoElement("Reloaded: ", r);
	
	var hExcepted = getInfoElement("Excepted", "");
	var tExcepted = document.createElement("table");
	tExcepted.setAttribute("border", "1");
	var tHeader3 = getTableRow("Third party", "# requests", true);
	tExcepted.appendChild(tHeader3);

	var countExcepted = 0;
	var countExceptedReq = 0;
	var exceptedItems = [];
	for (var tp in tabInfo["excepted"])
	{
		countExcepted++;
		var len = tabInfo["excepted"][tp].length;
		countExceptedReq += len;
		exceptedItems.push([tp, len]); 
	}
	
	exceptedItems.sort(function(a, b)
		{
			return a[0].localeCompare(b[0]);
		});
	for (var i = 0; i < exceptedItems.length; i++)
	{
		var tp = exceptedItems[i][0];
		var len = exceptedItems[i][1];
		var litp = getTableRow(tp, len, false);
		tExcepted.appendChild(litp);
	}
	
	var e = countExcepted + " domain";
	if (countExcepted > 1)
	{
		e += "s";
	}
	e += ", " + countExceptedReq + " request";
	if (countExceptedReq > 1)
	{
		e += "s";
	}
	var excepted = getInfoElement("Excepted: ", e);
	
	var divInfo = document.getElementById("tabinfo");
	
	while (divInfo.firstChild)
	{
	    divInfo.removeChild(divInfo.firstChild);
	}

	divInfo.appendChild(hInfo);
	divInfo.appendChild(site);
	divInfo.appendChild(lds);
	divInfo.appendChild(loc);
	if (!tabInfo["disabledSite"])
	{
		divInfo.appendChild(ldp);
	}

	divInfo.appendChild(loadtime);
	
	divInfo.appendChild(totaltp);

//	if (!tabInfo["disabledSite"] && !tabInfo["disabledPage"])
	{
		divInfo.appendChild(filtered);
		divInfo.appendChild(nocookies);
		divInfo.appendChild(reloaded);
		divInfo.appendChild(excepted);
	}

	var hr = document.createElement("hr");
	divInfo.appendChild(hr);
	
	if (countFiltered > 0)
	{
		var divFiltered = document.getElementById("thirdpartylist_filtered");
		
		while (divFiltered.firstChild)
		{
		    divFiltered.removeChild(divFiltered.firstChild);
		}
		
		divFiltered.appendChild(hFiltered);
		divFiltered.appendChild(tFiltered);
		var hr = document.createElement("hr");
		divFiltered.appendChild(hr);
	}
	
	if (countNoCookies > 0)
	{
		var divNoCookies = document.getElementById("thirdpartylist_nocookies");
		
		while (divNoCookies.firstChild)
		{
		    divNoCookies.removeChild(divNoCookies.firstChild);
		}
		
		divNoCookies.appendChild(hNoCookies);
		divNoCookies.appendChild(tNoCookies);
		var hr = document.createElement("hr");
		divNoCookies.appendChild(hr);	    
	}
	
	if (countReloaded > 0)
	{
		var divReloaded = document.getElementById("thirdpartylist_reloaded");

		while (divReloaded.firstChild)
		{
		    divReloaded.removeChild(divReloaded.firstChild);
		}

		divReloaded.appendChild(hReloaded);
		divReloaded.appendChild(tReloaded);
		var hr2 = document.createElement("hr");
		divReloaded.appendChild(hr2);
	}
	
	if (countExcepted > 0)
	{
		var divExcepted = document.getElementById("thirdpartylist_excepted");
		
		while (divExcepted.firstChild)
		{
		    divExcepted.removeChild(divExcepted.firstChild);
		}

		divExcepted.appendChild(hExcepted);
		divExcepted.appendChild(tExcepted);
	}
}

function updatePopup()
{
	chrome.windows.getCurrent(
		function (currentWindow)
		{
			//console.log(currentWindow);
			chrome.tabs.query({active: true, windowId: currentWindow.id}, 
				function(activeTabs)
				{
					// XXX: get all the necessary information here
					
					// XXX: get all options
					// 1. should highlight
					var highlightOptions = backgroundPage.getHighlightOptions();
					var shouldHighlight = highlightOptions[0];
					populateOptionsList(shouldHighlight);
					
					//console.log(activeTabs);
					var tabInfo = backgroundPage.getTabInfo(activeTabs[0].id);
					if (tabInfo)
					{
						displayTabInfo(tabInfo);
					}
				});

		});
}

window.addEventListener("DOMContentLoaded", function(event)
	{
		updatePopup();
	});
