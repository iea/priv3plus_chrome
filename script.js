if (typeof Priv3Plus == "undefined")
{
	var Priv3Plus = {
		receivedTabData: {},
		localTabData: {
			
			thirdParties: {},
			
			// TODO: clear these timeouts?
			postLoadTimeout: null,
			highlightTimeout: null,
			highlightTrialCount: 0,

			// XXX: highlighted elements
			highlightList: [],
			originalStyleMap: {},
			
			// XXX: for caching the info about third parties
			highlightThirdPartyList: {},
			
			reloadedItems: {},
		},
	};
}

(function() {

	function getNodeIfTargetIsHyperLink(node)
	{
		var el = node;
		while ((el != null) && el.tagName != "BODY") 
		{
			if(el.tagName == "A")
			{
				return el;
			}
			else
			{
				//console.log("clicked el: " + el.tagName);
			}
			el = el.parentNode;
		}
		return null;
	}
	
	var reload = function(elems, thirdParty)
	{
		var tab = Priv3Plus.localTabData;
		var rcvdTab = Priv3Plus.receivedTabData;
		
		if (!tab.reloadedItems[thirdParty])
		{
			tab.reloadedItems[thirdParty] = {};
		}
	
		for(var i = 0; i < elems.length; i++)
		{
			var e = elems[i];
			switch(e.tagName) 
			{
				case "IFRAME":
					if(e)
					{
						if (e.src != "" || e.contentWindow.document.location != "about:blank")
						{
							// XXX: init reloaded iframe items
							if (!tab.reloadedItems[thirdParty]["iframe"])
							{
								tab.reloadedItems[thirdParty]["iframe"] = [];
							}
							if (tab.reloadedItems[thirdParty]["iframe"].indexOf(e.src) == -1)
							{
								// XXX: add it to the list
								// and do not reload even if the user clicks again.
								tab.reloadedItems[thirdParty]["iframe"].push(e.src);
	
								// XXX: send message to background so that it can send
								// message to the script of the iframe with 
								// reload directive
								chrome.extension.sendMessage({msg: "iframeMain", src: e.src});

								var type = "";
								if (!e.getAttribute("priv3_clicked"))
								{
									type = "RELOADED_ELEMENT";
									e.setAttribute("priv3_reloaded", "1");
								}

								tab.highlightList.push([e, type, tab.originalStyleMap[e]]);

								//console.log("priv3plus reloading iframe: " + e.src);
								//console.log(e.parentNode.childNodes);
								/*var parent = e.parentNode;
								var e2 = window.document.createElement("iframe");
								e2.setAttribute("src", e.getAttribute("src"));
								e2.setAttribute("id", e.getAttribute("id"));
								e2.setAttribute("class", e.getAttribute("class"));
								e2.setAttribute("style", e.getAttribute("style"));
								e2.setAttribute("frameborder", e.getAttribute("frameborder"));
								e2.setAttribute("scrolling", e.getAttribute("scrolling"));
								e2.setAttribute("allowtransparency", e.getAttribute("allowtransparency"));
								if (e.getAttribute("priv3_main_element") == "1")
								{
									e2.setAttribute("priv3_main_element", "1");
								}
								else if (e.getAttribute("priv3_other_iframe") == "1")
								{
									e2.setAttribute("priv3_other_iframe", "1");
								}
								// XXX: Google Chrome does not allow us to do the reload
//								e.contentWindow.document.location.reload(true);
								// XXX: indicate we reloaded; change the highlight
								var type = "";
								if (e.getAttribute("priv3_clicked") == "1")
								{
									type = "CLICKED_ELEMENT";
									e2.setAttribute("priv3_clicked", "1");
								}
								else if (!e.getAttribute("priv3_clicked"))
								{
									type = "RELOADED_ELEMENT";
									e2.setAttribute("priv3_reloaded", "1");
								}

								tab.highlightList.push([e2, type, HighlightHelpers.getElementStyle(e2)]);

								parent.insertBefore(e2, e);
								parent.removeChild(e);
								//parent.appendChild(e2);
								elems[i] = e2;
								*/
							}
							else
							{
								//console.log("priv3: already reloaded iframe: " + e.src);
								var type = "";
								if (e.getAttribute("priv3_clicked") == "1")
								{
									type = "CLICKED_ELEMENT";
								}

								tab.highlightList.push([e, type, tab.originalStyleMap[e]]);

							}
						}
					}
					break;
				default:
					break;
			}
		}
		
		HighlightHelpers.toggleElementHighlights();
		
		chrome.extension.sendMessage({msg: "reloadedItems", tp: thirdParty, ri: tab.reloadedItems[thirdParty]});
	};
	
	function removeProtocol(url)
	{
		var url2 = "";
		if (url.indexOf("http://") == 0)
		{
			url2 = url.substring(7);
		}
		else if (url.indexOf("https://") == 0)
		{
			url2 = url.substring(8);
		}
		return url2;
	}
	
	// XXX: can only be called from the first party's frame
	function findIframeAndReload(url)
	{
		var tab = Priv3Plus.localTabData;
		var rcvdTab = Priv3Plus.receivedTabData;
		
		// XXX: we are now using the third party host as the thirdParty name rather than pre-defined thirdParty names
		var thirdParty = URLHelpers.getDomain(url, rcvdTab["hostnameToDomainMap"]);
		if (!thirdParty || (!rcvdTab["thirdParties"][thirdParty] && !ExceptionHelpers.isAllowedException(thirdParty, rcvdTab)))
		{
			thirdParty = URLHelpers.getDomain(window.document.location.href, rcvdTab["hostnameToDomainMap"]);
		}
	
		//console.log("thirdParty: " + thirdParty);
	
		var shouldHighlight = rcvdTab["highlight"];
		if(thirdParty)
		{
			var iframes = DOMHelpers.getElementsByMultipleTagNames(window.document, ["iframe"]);
			var iframe = null;
			//console.log(iframes);
	
			// XXX: search the clicked element in an iframe in the main document
			var elementInMainIframe = false;
			for(var i = 0; i < iframes.length; i++) 
			{
				//console.log("checking main iframes");
				//console.log(iframes[i]);
				// XXX: iframes of social widgets have to have a src; otherwise,
				// we won't be able reload them anyway
				//console.log(iframes[i].src);
				
				if(iframes[i].src == url)
				{
					//console.log("found iframe in main while intercepting");
					elementInMainIframe = true;
					iframe = iframes[i];
					break;
				}
				else
				{
					var src = removeProtocol(iframes[i].src);
					var src2 = removeProtocol(url);
					if (src == src2)
					{
						//console.log("found iframe in main while intercepting 2");
						elementInMainIframe = true;
						iframe = iframes[i];
						break;
					}
				}
			}
	
			// XXX: if iframe is still null, that means the clicked element may be in a child iframe
			// look for the child iframe that may contain the clicked element
			if (iframe == null)
			{
				elementInMainIframe = false;
				var childIframes = DOMHelpers.getChildIframes(iframes);
				//console.log("checking child iframes");
				for(var i = 0; i < childIframes.length; i++) 
				{
					if(childIframes[i].src == url)
					{
						//console.log("found child iframe while intercepting");
						elementInMainIframe = false;
						iframe = childIframes[i];
						break;
					}
					else
					{
						var src = removeProtocol(childIframes[i].src);
						var src2 = removeProtocol(url);
						if (src == src2)
						{
							//console.log("found child iframe while intercepting 2");
							elementInMainIframe = false;
							iframe = iframes[i];
							break;
						}
					}
				}
				
			}
				
			if (iframe != null && !iframe.getAttribute("priv3_clicked"))
			{
				//console.log("may reload iframe for thirdParty: " + thirdParty);
				if (rcvdTab["thirdParties"][thirdParty])
				{
					// XXX: we just need to set the user interaction to true, so that it can load or reload some
					// iframes or new windows without stripping the cookies
					chrome.extension.sendMessage({msg: "click", tp: thirdParty});
					//tab["thirdParties"][thirdParty].userInteraction = true;
					
					iframe.setAttribute("priv3_clicked", "1");
	
					if (!elementInMainIframe)
					{
						// XXX: this is a heuristic to determine whether the click was to an advertisement or 
						// a social media widget
						// advertisements can be loaded within multiple nested iframes; hence, the check
						// above within the child iframes
						// if so, it's probably an advertisement; therefore, no need to reload the parent iframe
						console.log("not reloading iframe due to child iframes: " + thirdParty);
					}
					else
					{
						// XXX: this element is probably a social media widget (and found in an iframe in the main
						// document). For preserving social networking functionality after the user interaction,
						// we need to reload the iframes belonging to this third party
						// XXX: this is an element in an iframe in the main document or an iframe
						// area the user clicked; reload 
						console.log("reloading iframe(s) in main document: " + thirdParty);
						//console.log(tab["thirdParties"][thirdParty].savedElems);
						reload(tab["thirdParties"][thirdParty].savedElems, thirdParty);
					}
				}
				else if (ExceptionHelpers.isAllowedException(thirdParty, rcvdTab))
				{
					// XXX: it was already loaded with cookies!
					// no need to reload anything else.
					//HighlightHelpers.highlight(iframe, "CLICKED_ELEMENT", tab);
					//iframe.setAttribute("priv3_clicked", "1");
				}
				else
				{
					//console.log("nothing to reload because thirdParty had no cookies (i.e., ["thirdParties"][thirdParty] == null)");
				}
			}
			else
			{
			}
		}
	
	};
	
	function findLinkNodeAndHighlight(url)
	{
		var tab = Priv3Plus.localTabData;
		var rcvdTab = Priv3Plus.receivedTabData;
		
		// XXX: we are now using the third party host as the thirdParty name rather than pre-defined thirdParty names
		var thirdParty = URLHelpers.getDomain(url, rcvdTab["hostnameToDomainMap"]);
		// XXX: the clicked element was not in an iframe
		// XXX: must be in the main
		console.log("no iframe for thirdParty: " + thirdParty);
//				HighlightHelpers.highlight(linkNode, "CLICKED_ELEMENT");
//				reload(tab["thirdParties"][thirdParty].savedElemURLs, thirdParty, tab);
		var shouldHighlight = rcvdTab["highlight"];
		var elems = DOMHelpers.getElementsByMultipleTagNames(window.document, ["img"]);
		for (var i = 0; i < elems.length; i++)
		{
			if (elems[i].src == url)
//			var elemThirdParty = URLHelpers.getElemThirdParty(elems[i]);
//			if (rcvdTab["thirdParties"][thirdParty] && elemThirdParty == thirdParty)
			{
				//console.log(elems[i]);
				/*
				if (shouldHighlight)
				{
					HighlightHelpers.highlight(elems[i], "CLICKED_ELEMENT");
				}
				*/
				//console.log("aa: " + elems[i].src);
				tab.highlightList.push([elems[i], "CLICKED_ELEMENT", tab.originalStyleMap[elems[i]]]);
				
			}
		}
		
		HighlightHelpers.toggleElementHighlights();

	}
	
	function interceptClick(event, tab)
	{
		//console.log("intercepting click...");
		if (event.button != 0 && event.button != 1)
		{
			return;
		}
	
		var url = null;
		var target = event.target;
		//console.log(target);
		var doc = target.ownerDocument;
		//console.log(doc);
		var type = "";
	
		// XXX: not necessary anymore: the heuristic for not reloading advertisement iframes
		// is to check for child iframe rather than images
		
		var linkNode = getNodeIfTargetIsHyperLink(target);
		if(linkNode && linkNode.href && target.src)
		{
			url = target.src;
			type = "linknode";
			//console.log("linknode; intercepting url: " + url);
			//url = window.location.href;
		}
		else
		{
			url = doc.location.href;
			type = "iframe";
			//console.log("some url; intercepting url: " + url);
		}
		
		
		//url = doc.location.href;
		
		chrome.extension.sendMessage({msg: "reload", url: url, type: type});
	
		//	console.log(window.document);
	//	chrome.extension.sendMessage(url);
	}
	
	var checkIframe = function(iframe, rcvdTab)
	{
		//console.log("checking: " + iframe.id + " " + iframe.name + " " + iframe.src);
		var childIframes = [];
		var elemsIframe = DOMHelpers.getElementsByMultipleTagNames(iframe.contentWindow.document, DOMHelpers.tagsWithIframe);
		//console.log("elemsIframe len: " + elemsIframe.length);
		for (var j = 0; j < elemsIframe.length; j++)
		{
			 var elem = elemsIframe[j];
			 if (elem.tagName == "IFRAME")
			 {
				 childIframes.push(elem);
			 }
			 var thirdParty = URLHelpers.getElemThirdParty(elem, rcvdTab["hostnameToDomainMap"]);
	//		console.log(thirdParty);
			 if (thirdParty && rcvdTab["thirdParties"][thirdParty] && rcvdTab["thirdParties"][thirdParty].cookiesRemoved)
			 {
				return true;
			 }
	
		}
		// XXX: if we are here, we couldn't find any elements to a third party
		// in this iframe. It may still be the case that the child iframes
		// may have had an element
	//		console.log("checking child iframes for: " + iframe.id);
		var res = false;
		for (var k = 0; k < childIframes.length; k++)
		{
	//			console.log("childIframe: " + childIframes[k].id);
			res |= checkIframe(childIframes[k], rcvdTab);
		}
		
		// XXX: if we are here, none of the elements were from a third party
		// none of the child iframes didn't have any elements from a third party
		// either.
		return res;
	
	};
	
	var DOMHelpers = {
	
		tagsWithIframe : ["iframe", "img", "script", "link", "embed", "object"],
		tags : ["img", "script", "link", "embed", "object"],
		tagsVisibleExceptIframe : ["img", "embed", "object"],
		
		getElementsByMultipleTagNames : function(doc, tagnames)
		{
			//console.log(tagnames);
			var e = [];
			if (doc)
			{
				for (var t in tagnames)
				{
					//console.log(tagnames[t]);
					var elems = doc.getElementsByTagName(tagnames[t]);
					//console.log(tagnames[t] + " " + elems.length);
					for (var j = 0; j < elems.length; j++)
					{
						e.push(elems[j]);
					}
				}
			}
			return e;
		},
		
		getChildIframes : function(iframes)
		{
			var childIframes2 = [];
			for (var i = 0; i < iframes.length; i++)
			{
				var iframe = iframes[i];
				if (!iframe || !iframe.contentWindow)
				{
					continue;
				}
				//console.log(iframe.contentWindow.document);
				var childIframes = iframe.contentWindow.document.getElementsByTagName("iframe");
				for (var j = 0; j < childIframes.length; j++)
				{
					childIframes2.push(childIframes[j]);
				}
			}
	
			if (childIframes2.length > 0)
			{
				return childIframes2.concat(this.getChildIframes(childIframes2));
			}
			
			return childIframes2;
		}
	
	};
	
	var URLHelpers = {
	
		getElemURL: function(elem)
		{
			if (elem.tagName == "OBJECT" && elem.data)
			{
				return elem.data;
			}
			else if (elem.tagName == "LINK" && elem.href)
			{
				return elem.href;
			}
			else if (elem.src)
			{
				return elem.src;
			}
			return null;
		},
		
		getElemThirdParty: function(elem, hostnameToDomainMap)
		{
			var url = this.getElemURL(elem);
			if (url)
			{
				return hostnameToDomainMap[this.getHostname(url)];
			}
			return null;
		},
	
		getHostname: function(url) 
		{
			var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/]+)', 'im');
			var match = url.match(re);
			if(match)
			{
				return match[1].toString();
			}
			return null;
		},
		
		getDomain: function(url, hostnameToDomainMap)
		{
			return hostnameToDomainMap[this.getHostname(url)];		
		},
		
	};
	
	var ExceptionHelpers = {
		
		isAllowedException : function(thirdPartyDomain, tab)
		{
			if (tab.cachedAllowedExceptionHosts && tab.cachedAllowedExceptionHosts.indexOf(thirdPartyDomain) != -1)
			{
				return true;
			}
			return false;
		},
		
	
	};
	
	
	var HighlightHelpers = {
		// if the element is an iframe, recursively retrieve all third party names of it
		getAllThirdParties : function(e, infoId)
		{
			var tab = Priv3Plus.localTabData;
			var rcvdTab = Priv3Plus.receivedTabData;
			
			if (tab.highlightThirdPartyList[infoId])
			{
				// XXX: cache of all the third parties for future use
				//console.log("using cached third parties info")
				return tab.highlightThirdPartyList[infoId];
			}
	
			var thirdParties = [];
			var thirdPartiesOther = [];
			var thirdPartiesAllowedExceptions = [];
			var iframes = [];
			if (e.tagName == "IFRAME")
			{
				//console.log("parent iframe: " + e.src);
		
				iframes.push(e);
				//console.log(e);
	
				iframes = iframes.concat(DOMHelpers.getChildIframes([e]));
	
				var len = iframes.length;
				//console.log(len);
				for (var i = 0; i < len; i++)
				{
					var iframe = iframes[i];
					var p = URLHelpers.getElemThirdParty(iframe, rcvdTab["hostnameToDomainMap"]);
					//console.log("iframe: " + iframe.src + " " + p);
					if (p != null && p != "" && p != rcvdTab.site)
					{
						//console.log("third party iframe: " + p + " " + tab["thirdParties"][p]);
						if (ExceptionHelpers.isAllowedException(p, rcvdTab))
						{
							if (thirdPartiesAllowedExceptions.indexOf(p) == -1)
							{
								thirdPartiesAllowedExceptions.push(p);
							}
						}
						else if (rcvdTab["thirdParties"][p] && rcvdTab["thirdParties"][p].cookiesRemoved)
						{
							if (thirdParties.indexOf(p) == -1)
							{
								thirdParties.push(p);
							}
						}
						else if (rcvdTab["thirdParties"][p] && !rcvdTab["thirdParties"][p].cookiesRemoved)
						{
							if (thirdPartiesOther.indexOf(p) == -1)
							{
								thirdPartiesOther.push(p);
							}
						}
					}
	
					//console.log(iframe);
					if (!iframe || !iframe.contentWindow)
					{
						continue;
					}
					var elems = DOMHelpers.getElementsByMultipleTagNames(iframe.contentWindow.document, DOMHelpers.tags);
					//console.log("elems length: " + elems.length);
					
					for (var j = 0; j < elems.length; j++)
					{
						var elem = elems[j];
						var p = URLHelpers.getElemThirdParty(elem, rcvdTab["hostnameToDomainMap"]);
						//console.log("elem in iframe: " + elem.src + " " + p);
						//console.log(elem);
						//console.log(tab["thirdParties"][p]);
						if (p != null && p != "" && p != rcvdTab.site)
						{
							// console.log("third party in an iframe: " + p + " " + tab["thirdParties"][p]);
							if (ExceptionHelpers.isAllowedException(p, rcvdTab))
							{
								if (thirdPartiesAllowedExceptions.indexOf(p) == -1)
								{
									thirdPartiesAllowedExceptions.push(p);
								}
							}
							else if (rcvdTab["thirdParties"][p] && rcvdTab["thirdParties"][p].cookiesRemoved)
							{
								if (thirdParties.indexOf(p) == -1)
								{
									thirdParties.push(p);
								}
							}
							else if (rcvdTab["thirdParties"][p] && !rcvdTab["thirdParties"][p].cookiesRemoved)
							{
								if (thirdPartiesOther.indexOf(p) == -1)
								{
									thirdPartiesOther.push(p);
								}
							}
						}
					}
					
				}
			}
			else
			{
				var p = URLHelpers.getElemThirdParty(e, rcvdTab["hostnameToDomainMap"]);
				//console.log(e.src + " " + p);
				if (p != null && p != "" && p != tab.site)
				{
					//console.log("third party element: " + p);
					if (ExceptionHelpers.isAllowedException(p, rcvdTab))
					{
						if (thirdPartiesAllowedExceptions.indexOf(p) == -1)
						{
							thirdPartiesAllowedExceptions.push(p);
						}
					}
					else if (rcvdTab["thirdParties"][p] && rcvdTab["thirdParties"][p].cookiesRemoved)
					{
						if (thirdParties.indexOf(p) == -1)
						{
							thirdParties.push(p);
						}
					}
					else if (rcvdTab["thirdParties"][p] && !rcvdTab["thirdParties"][p].cookiesRemoved)
					{
						if (thirdPartiesOther.indexOf(p) == -1)
						{
							thirdPartiesOther.push(p);
						}
					}
			  }
			}
			var thirdPartiesAll = [thirdParties, thirdPartiesOther, thirdPartiesAllowedExceptions]; 
			tab.highlightThirdPartyList[infoId] = thirdPartiesAll;
			return thirdPartiesAll;
		},
	  
		getThirdPartiesText : function(list)
		{
			var text = "";
			var tooltip = "";
			for (var i = 0; i < list.length; i++)
			{
				tooltip += list[i] + ", ";
				if (i < 3)
				{
					text += list[i] + ", ";
				}
			}
			
			if (list.length >= 4)
			{
				var rem = list.length - 3;
				text += "(" + rem + " more)";
			}
			else
			{
				text = text.substring(0, text.length-2);
			}
	
			tooltip = tooltip.substring(0, tooltip.length -2);
			
			return [text, tooltip];
		},
	  
		getTextInfo : function(e, infoId)
		{
			var allThirdParties = this.getAllThirdParties(e, infoId);
			var thirdParties = allThirdParties[0];
			var thirdPartiesOther = allThirdParties[1];
			var thirdPartiesAllowedExceptions = allThirdParties[2];
			//console.log(allThirdParties);
			
			var text = "";
			var tooltip = "";
	
			if (thirdParties.length == 0 && thirdPartiesOther.length == 0 && thirdPartiesAllowedExceptions.length == 0)
			{
	//			text = "(Potential third party content)";
	//			tooltip = "Potential third party content (no cookies sent)";
			}
			else
			{
				if (thirdParties.length > 0)
				{
					text = "Third party content (cookies removed) (" + thirdParties.length + "): ";
					tooltip = "Third party content (cookies removed): ";
	
					var info = this.getThirdPartiesText(thirdParties);
					text += info[0] + "&&&&&";
					tooltip += info[1] + "\n\n";
				}
				else if (e.getAttribute("priv3_iframe_element") == "1")
				{
					text = "Third party content (cookies removed): some that cannot be listed...)" + "&&&&&";
					tooltip = "Third party content (cookies removed): some that cannot be listed, because Google Chrome does not allow access to cross-domain IFRAME content.";
					tooltip += " No worries; all non-excepted cookies were still removed from the requests.)" + "\n\n";
				}
				
				if (thirdPartiesOther.length > 0)
				{
					text += "Third party content (no cookies) (" + thirdPartiesOther.length + "): ";
					tooltip += "Third party content (no cookies): ";
	
					var info = this.getThirdPartiesText(thirdPartiesOther);
					text += info[0] + "&&&&&";
					tooltip += info[1] + "\n\n";
				}
				
				if (thirdPartiesAllowedExceptions.length > 0)
				{
					text += "Third party content (allowed exceptions) (" + thirdPartiesAllowedExceptions.length + "): ";
					tooltip += "Third party content (allowed exceptions): ";
	
					var info = this.getThirdPartiesText(thirdPartiesAllowedExceptions);
					text += info[0] + "&&&&&";
					tooltip += info[1] + "\n\n";
				}
				
				text = text.substring(0, text.length-5);
				tooltip = tooltip.substring(0, tooltip.length-2);
			}
	
			return [text, tooltip];
	  },
	  
		generateInfoTextId : function(e)
		{
			try
			{
				var infoId = "priv3_overlay_" + e.tagName + "_" + e.id + "_";
				if (e.src)
				{
					infoId += e.src;
				}
				else if (e.data)
				{
					infoId += e.data;
				}
				return infoId;
			}
			catch (e)
			{
			}
			return null;
	  },
	  
		drawInfo : function(elem, event)
		{
			var tab = Priv3Plus.receivedTabData;
			
			var shouldHighlight = tab["highlight"];
			if (!shouldHighlight)
			{
				return;
			}
	
			var infoId = this.generateInfoTextId(elem);
			if (infoId == null)
			{
				return;
			}
	
			var doc = elem.ownerDocument.defaultView.top.document;
			var oldInfo = doc.getElementById(infoId);
	
			if (oldInfo)
			{
				// XXX: we already drew a tab for this object
				return;
			}
					
			var textContent = this.getTextInfo(elem, infoId);
	
			var text = textContent[0];
			var tooltip = textContent[1];
			
			if (text == "" && tooltip == "")
			{
				// nothing to show in the box
				//console.log("nothing to show?");
				return;
			}
	
			//console.log(infoId);
	
			var info = doc.createElement("div");
	
			info.setAttribute("id", infoId);
			info.setAttribute("align", "left");
			
			var tokens = text.split("&&&&&");
			for (var i = 0; i < tokens.length; i++)
			{
				var textnode = doc.createTextNode(tokens[i]);
				var br = doc.createElement("br");
				info.appendChild(textnode);
				info.appendChild(br);
			}
	
			info.setAttribute("title", tooltip);
	
			info.style.setProperty("position", "fixed", "important");
			info.style.setProperty("display", "block", "important");
			info.style.setProperty("background", "white", "important");
			info.style.setProperty("border-style", "solid", "important");
			info.style.setProperty("border-color", "red", "important");
			info.style.setProperty("border-width", "3px", "important");
			info.style.setProperty("line-height", "%100", "important");
			info.style.setProperty("color", "black", "important");
			info.style.setProperty("font-size", "10px", "important");
			info.style.setProperty("font-family", "Arial,sans-serif", "important");
			info.style.setProperty("z-index", "10000", "important");
	
			var rect = elem.getBoundingClientRect();
			info.style.left = info.style.setProperty("left", (event.clientX) + "px", "important");
			info.style.top = info.style.setProperty("top", (event.clientY) + "px", "important");
	//		console.log(info.style);
	
			info.style.setProperty("opacity", "1", "important");
			
			//doc.documentElement.appendChild(tab);
			doc.body.appendChild(info);
	
			setTimeout(function() { HighlightHelpers.undrawInfo(elem);}, 3000);
			event.preventDefault();
			event.stopPropagation();
			
		},
	  
		undrawInfo : function(elem)
		{
			var infoId = this.generateInfoTextId(elem);
			if (infoId == null)
			{
				return;
			}
			
			var doc = elem.ownerDocument.defaultView.top.document;
			var oldtab = doc.getElementById(infoId);
			if (oldtab)
			{
				//doc.documentElement.removeChild(oldtab);
				doc.body.removeChild(oldtab);
			}
		},
		
		delayDrawingInfo : function(elem)
		{
			var timeout = null;
			elem.addEventListener("mouseover", function(event)
				{
					timeout = setTimeout(function() { HighlightHelpers.drawInfo(elem, event);}, 1000);
				}, false);
			
			elem.addEventListener("mouseout", function()
				{
					clearTimeout(timeout);
				}, false);
		},
		
		getElementStyle : function(elem)
		{
			var tab = Priv3Plus.localTabData;
			var wnd = elem.ownerDocument.defaultView;
			var style = wnd.getComputedStyle(elem, null);
			var origStyle = {};
			origStyle.outline = style.getPropertyValue("outline");
			origStyle.outlineColor = style.getPropertyValue("outline-color");
			origStyle.outlineOffset = style.getPropertyValue("outline-offset");
			origStyle.outlineStyle = style.getPropertyValue("outline-style");
			origStyle.outlineWidth = style.getPropertyValue("outline-width");
			//console.log(origStyle);
			tab.originalStyleMap[elem] = origStyle;
			return origStyle;
		},
		
		// XXX: returns if element visible and its style
		isElementVisible : function(elem)
		{
			var style = this.getElementStyle(elem);
			if (style != null)
			{
				if (style.getPropertyValue("display") == "none")
				{
					return [false, style];
				}
	
				var width = style.getPropertyValue("width");
				var height = style.getPropertyValue("height");			
				if (width == "0px" || height == "0px" || width == "0" || height == "0")
				{
					return [false, style];
				}
			}
			return [true, style];
		},
	
		setOutline : function(e, type)
		{
			var linetype = "3px dashed ";
			
			switch(type)
			{
				case "LOADED_ELEMENT_MAIN":
					// RED
					e.style.outline = linetype + "#ff0000";
					break;
				case "LOADED_IFRAME_ELEMENT":
					// ORANGE
					e.style.outline = linetype + "#ffa500";
					break;
				case "LOADED_IFRAME_OTHER":
				case "LOADED_OTHER":
					// YELLOW
					e.style.outline = linetype + "#fff000";
					break;
				case "CLICKED_ELEMENT":
					// PURPLE
					e.style.outline = linetype + "#f000ff";
					break;
				case "RELOADED_ELEMENT":
					// TURQUOISE
					e.style.outline = linetype + "#00f0ff";
					break;
				case "ALLOWED_EXCEPTION":
					// GREEN
					e.style.outline = linetype + "#00ff00";
					break;
				case "SELECTIVELY_RELOADED_ELEMENT":
					// BLUE
					e.style.outline = "solid 3px " + "#0000ff";
					break;
			}
			
			e.style.outlineOffset = "-3px";
		
		},
		
		unsetOutline : function(elem, style)
		{
			elem.style["outline"] = style.outline;
			elem.style["outline-color"] = style.outlineColor;
			elem.style["outline-offset"] = style.outlineOffset;
			elem.style["outline-style"] = style.outlineStyle;
			elem.style["outline-width"] = style.outlineWidth;
		},
	
		highlight : function(e, type)
		{
			if (e.tagName == "SCRIPT")
			{
				return;
			}
			if (!e.getAttribute("priv3_listeners")
				&& type != "RELOADED_ELEMENT"
				&& type != "SELECTIVELY_RELOADED_ELEMENT"
				&& type != "CLICKED_ELEMENT")
			{
				this.delayDrawingInfo(e);
				e.setAttribute("priv3_listeners", "1");
			}
	
			this.setOutline(e, type);
	
		},
		
		toggleElementHighlights : function()
		{
			var tab = Priv3Plus.localTabData;
			var rcvdTab = Priv3Plus.receivedTabData;
			
			//console.log("toggling element highlights...");
			var shouldHighlight = rcvdTab["highlight"];
			//console.log("should highlight: " + shouldHighlight);

			var list = tab.highlightList;
			if (!list)
			{
				return;
			}

			var highlightOptions = rcvdTab["highlightOptionsList"];
			//console.log(highlightOptions);
			
			for (var i = 0; i < list.length; i++)
			{
				var elem = list[i][0];
				var type = list[i][1];
				var style = list[i][2];
				
				var selectedHighlight = true;

				switch(type)
				{
					case "LOADED_ELEMENT_MAIN":
						if (!highlightOptions["main"])
						{
							selectedHighlight = false;
						}
						break;
					case "LOADED_IFRAME_ELEMENT":
						if (!highlightOptions["iframe"])
						{
							selectedHighlight = false;
						}
						break;
					case "LOADED_IFRAME_OTHER":
					case "LOADED_OTHER":
						if (!highlightOptions["other"])
						{
							selectedHighlight = false;
						}
						break;
					case "CLICKED_ELEMENT":
						if (!highlightOptions["clicked"])
						{
							selectedHighlight = false;
						}
						break;
					case "RELOADED_ELEMENT":
						if (!highlightOptions["reloaded"])
						{
							selectedHighlight = false;
						}
						break;
					case "ALLOWED_EXCEPTION":
						if (!highlightOptions["excepted"])
						{
							selectedHighlight = false;
						}
						break;
				}
	
				if (shouldHighlight && selectedHighlight)
				{
					//console.log("set outline: " + shouldHighlight + " " + selectedHighlight);
					this.highlight(elem, type);
				}
				else
				{
					//console.log("unset outline: " + shouldHighlight + " " + selectedHighlight);
					this.unsetOutline(elem, style);
				}
			}
		},
	};
	
	
	var saveAndHighlightThirdPartyContent = function()
	{
		var tab = Priv3Plus.localTabData;
		
		if (tab.highlightTrialCount == 3)
		{
			// XXX: saved elements; highlight them
			HighlightHelpers.toggleElementHighlights();
			return;
		}
		console.log("running postload save elements and highlight: " + tab.loc + " " + tab.highlightTrialCount);
		tab.highlightTrialCount++;

		var rcvdTab = Priv3Plus.receivedTabData;

		var doc = window.document;
	
		// XXX: check whether an element in the main document has been loaded from a third party
		var emain = [];
		var iframes = DOMHelpers.getElementsByMultipleTagNames(doc, ["iframe"]);
		var others = DOMHelpers.getElementsByMultipleTagNames(doc, DOMHelpers.tags);
	//	console.log("iframes len: " + iframes.length + " others len: " + others.length);
		emain = emain.concat(iframes);
		emain = emain.concat(others);
		for (var i = 0; i < emain.length; i++)
		{
	//		console.log("main: " + emain[i].id + " " + emain[i].src);
			var thirdParty = URLHelpers.getElemThirdParty(emain[i], rcvdTab["hostnameToDomainMap"]);
	//		console.log("thirdParty: " + thirdParty);
			if (thirdParty && rcvdTab["thirdParties"][thirdParty]
				&& rcvdTab["thirdParties"][thirdParty].cookiesRemoved
				)
			{
				// save it and mark it for the user
				tab["thirdParties"][thirdParty].savedElems.push(emain[i]);
				
				if (!emain[i].getAttribute("priv3_clicked")
					&& !emain[i].getAttribute("priv3_reloaded"))
				{
					var type = "LOADED_ELEMENT_MAIN";
					
					// XXX: used when toggling highlight on/off
					tab.highlightList.push([emain[i], type, HighlightHelpers.getElementStyle(emain[i])]);
					emain[i].setAttribute("priv3_main_element", "1");
					//console.log(emain[i].src);
				}
			}
			
			else if (thirdParty && ExceptionHelpers.isAllowedException(thirdParty, rcvdTab))
			{
				emain[i].setAttribute("priv3_allowed_exception", "1");
				
				var type = "ALLOWED_EXCEPTION";
	
				// XXX: used when toggling highlight on/off
				tab.highlightList.push([emain[i], type, HighlightHelpers.getElementStyle(emain[i])]);
	
			}
			
		}
	
		
		// XXX: request must be from for or within an iframe
		// XXX: we got iframes above; now get all possible nested iframes
		// and the actual parents, because they may be javascript-generated, other frames (i.e., with no source)
		 for (var i = 0; i < iframes.length; i++)
		 {
			 var iframe = iframes[i];
			 if (iframe.getAttribute("priv3_main_element") == "1"
				 || iframe.getAttribute("priv3_clicked") == "1"
				 || iframe.getAttribute("priv3_reloaded") == "1")
			 {
				 continue;
			 }
			 //console.log(iframe);
			 
			 var result = checkIframe(iframe, rcvdTab);
	//		 console.log(result);
			 if (result == true)
			 {
				 // XXX: found our iframe that either itself or its children
				 // caused a request to a third party; save all
				 //console.log("found responsible iframe: " + iframe.id);
				 
				 var elems = DOMHelpers.getElementsByMultipleTagNames(iframe.contentWindow.document, DOMHelpers.tagsWithIframe);
				 //console.log(elems.length);
				 for (var j = 0; j < elems.length; j++)
				 {
					var elem = elems[j];
					var thirdParty = URLHelpers.getElemThirdParty(elem, rcvdTab["hostnameToDomainMap"]);
					if (thirdParty && rcvdTab["thirdParties"][thirdParty] && rcvdTab["thirdParties"][thirdParty].cookiesRemoved)
					{
						tab["thirdParties"][thirdParty].savedElems.push(elem);
		
						elem.setAttribute("priv3_iframe_element", "1");
						// XXX: highlight the iframe containing this request, only if:
						// 1) the request had a cookie attached (and got stripped) (i.e., existence of tab["thirdParties"][thirdParty])
						// 2) the iframe didn't get highlighted before (i.e., priv3_iframe_element was not set yet)
						if (!iframe.getAttribute("priv3_iframe_element"))
						{
							var type = "LOADED_IFRAME_ELEMENT";
							 
							// XXX: used when toggling highlight on/off
							tab.highlightList.push([iframe, type, HighlightHelpers.getElementStyle(iframe)]);
		
							iframe.setAttribute("priv3_iframe_element", "1");
							//console.log("highlightable iframe because of an element: " + elem.src + " " + thirdParty + " " + iframe.id);
						}
					}
					
					else if (thirdParty && ExceptionHelpers.isAllowedException(thirdParty, rcvdTab))
					{
						 elem.setAttribute("priv3_allowed_exception", "1");
						 
						 var type = "ALLOWED_EXCEPTION";
						 
						// XXX: used when toggling highlight on/off
						tab.highlightList.push([elem, type, HighlightHelpers.getElementStyle(elem)]);
					}	 
				}
			}
			
		}
		
		// XXX: just highlight other iframes that may potentially have third party content
		// (e.g., javascript from a third party may have generated an iframe (twitter timeline))
		// the cleanest way to do this is to have a taint-propagation from script to generated content,
		// but we are not doing fancy things regarding taint-propagation, so this should be ok for
		// highlighting potential third party content; the user can decide to interact or not, and then 
		// we'll identify third party content (and reload relevant stuff)--which is our focus anyway
		// XXX: make sure that the iframe is actually not from first party
		for (var i = 0; i < iframes.length; i++)
		{
			var iframe = iframes[i];
			// XXX: don't overwrite what we have highlighted above
			if (iframe.getAttribute("priv3_main_element") == "1"
				|| iframe.getAttribute("priv3_iframe_element") == "1"
				|| iframe.getAttribute("priv3_allowed_exception") == "1"
				|| iframe.getAttribute("priv3_clicked") == "1"
				|| iframe.getAttribute("priv3_reloaded") == "1")
			{
				continue;
			}
			var thirdParty = URLHelpers.getElemThirdParty(iframe, rcvdTab["hostnameToDomainMap"]);
			//console.log("other iframe: " + thirdParty + " " + rcvdTab.site)
			if (thirdParty != rcvdTab.site)
			{
				// save it and mark it for the user
				// XXX: iframes with no cookies are also saved, so that we can mark and reload
				// them when interacted
				// XXX: no harm for other items that are non-reloadable, because reload() checks
				// whether the element can be reloaded (i.e., iframe)
				if (thirdParty)
				{
					tab["thirdParties"][thirdParty].savedElems.push(iframe);
				}

				var type = "LOADED_IFRAME_OTHER";
		
				// XXX: used when toggling highlight on/off
				tab.highlightList.push([iframe, type, HighlightHelpers.getElementStyle(iframe)]);
		
				iframe.setAttribute("priv3_other_iframe", "1");
			}
		}
		
		var others = DOMHelpers.getElementsByMultipleTagNames(doc, DOMHelpers.tagsVisibleExceptIframe);
	//	console.log("others length: " + others.length);
		for (var i = 0; i < others.length; i++)
		{
			var other = others[i];
			// XXX: don't overwrite what we have highlighted above
			if (other.getAttribute("priv3_main_element") == "1"
				|| other.getAttribute("priv3_iframe_element") == "1"
				|| other.getAttribute("priv3_allowed_exception") == "1"
				|| other.getAttribute("priv3_clicked") == "1"
				|| other.getAttribute("priv3_reloaded") == "1")
			{
				continue;
			}
			var thirdParty = URLHelpers.getElemThirdParty(other, rcvdTab["hostnameToDomainMap"]);
			//console.log(other);
			//console.log("other element: " + thirdParty + " " + rcvdTab.site)
			if (thirdParty != rcvdTab.site)
			{
				var type = "LOADED_OTHER";
		
				// XXX: used when toggling highlight on/off
				tab.highlightList.push([other, type, HighlightHelpers.getElementStyle(other)]);
		
				other.setAttribute("priv3_other", "1");
			}
		}		
		
		var t = 1000;// * tab.highlightTrialCount;
		tab.highlightTimeout = window.setTimeout(function() {saveAndHighlightThirdPartyContent();}, t);
	};
	
	function postLoadPage()
	{
		var tab = Priv3Plus.localTabData;
		if (tab["postLoadPageRunning"])
		{
			return;
		}
		var win = window;
		// XXX: most stable way to figure out whether a page has fully loaded, including iframes
		if (win && win.performance && win.performance.timing && win.performance.timing.loadEventEnd > 0)
		{
			console.log("running post load for: " + tab.loc);
			tab["postLoadPageRunning"] = true;
			// XXX: timer for page load
			var t = win.performance.timing.loadEventStart - win.performance.timing.navigationStart;
			console.log("Time to load (" + tab.loc + "): " + t + " ms");
			chrome.extension.sendMessage({msg: "loadTime", lt: t}, 
				function(rcvdTab)
				{
					Priv3Plus.receivedTabData = rcvdTab;
					// XXX: init the local tab data with third parties
					var thirdParties = rcvdTab["thirdParties"];
					for (var tp in thirdParties)
					{
						Priv3Plus.localTabData["thirdParties"][tp] = {};
						Priv3Plus.localTabData["thirdParties"][tp].savedElems = [];
					}
					//console.log(Priv3Plus.receivedTabData);
					//console.log(Priv3Plus.localTabData);
					if (!rcvdTab.disabled)
					{
						// XXX: saving and highlighting content for reloading
						saveAndHighlightThirdPartyContent();
					}
				});
			
		}
		else
		{
			//console.log("resetting post load timer");
			tab["postLoadTimeout"] = window.setTimeout(function() {postLoadPage();}, 5000);
		}
	}
	
	
	// XXX: capture the clicks
	// the main frame does not capture click on an iframe
	// capture the clicks in each iframe separately
	// then, coordinate with the background page to determine which iframe was clicked
	// the background page then directs the main frame to find the iframe and reload
	// the third party if necessary
	window.addEventListener("click", function(event) { interceptClick(event); }, true);

	window.addEventListener("load", function(event)
	{
		var wloc = window.document.location;
		var data = {};
		// XXX: only if this is the main frame, run the postLoadPage()
		if (wloc.ancestorOrigins.length == 0)
		{
			Priv3Plus.localTabData["loc"] = wloc.href;
			postLoadPage();
			
			// XXX: only the main frame should receive the tab info
			chrome.extension.onConnect.addListener(
				function(port)
				{
					//console.log(port.name);
					port.onMessage.addListener(
						function(data)
						{
							//console.log(data);
							if (data.msg == "reload")
							{
								//console.log(data);
								if (data.type == "iframe")
								{
									findIframeAndReload(data.url);
								}
								else if (data.type == "linknode")
								{
									//console.log("reloading linknode: " + data.url);
									findLinkNodeAndHighlight(data.url);
								}
							}
							else if (data.msg == "updateHighlight")
							{
								//console.log(data.sh);
								//console.log(data.hol);
								Priv3Plus.receivedTabData["highlight"] = data.sh;
								Priv3Plus.receivedTabData["highlightOptionsList"] = data.hol;
								//console.log(Priv3Plus.receivedTabData);
								HighlightHelpers.toggleElementHighlights();
							}
						});
				});
		}
		else
		{
			chrome.extension.onConnect.addListener(
				function(port)
				{
					//console.log(port);
					port.onMessage.addListener(
						function(data)
						{
							//console.log(data);
							if (data["iframeMain"] && window.location.href == data.url)
							{
								//console.log("reloading iframe: " + window.location);
								window.location.reload(true);
							}
							else
							{
								//console.log("something else...");
							}
						});
				});
		}

	}, true);

})();
