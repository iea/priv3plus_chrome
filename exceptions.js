var backgroundPage = chrome.extension.getBackgroundPage();

function addException()
{
	var tpfield = document.getElementById("thirdparty");

	var sitefield = document.getElementById("site");
	
	if (tpfield.value != "" && sitefield.value != "")
	{
	    backgroundPage.addException(tpfield.value, sitefield.value);
	}
	tpfield.value = "";
	sitefield.value = "";
	
	populateExceptionsList();
}

function addDisabledSite()
{
    var sfield = document.getElementById("disabledsite");
    
    if (sfield.value != "")
    {
        backgroundPage.addDisabledSite(sfield.value);
    }
    sfield.value = "";
    
    populateDisabledSitesAndPages();
}

function addDisabledPage()
{
    var pfield = document.getElementById("disabledpage");
    
    if (pfield.value != "")
    {
        backgroundPage.addDisabledPage(pfield.value);
    }
    pfield.value = "";
    
    populateDisabledSitesAndPages();
}

function removeDisabledSite(site)
{
    backgroundPage.removeDisabledSite(site);
    populateDisabledSitesAndPages();    
}

function removeDisabledPage(url)
{
    backgroundPage.removeDisabledPage(url);
    populateDisabledSitesAndPages();    
}

function clearFields()
{
	var tpfield = document.getElementById("thirdparty");
	tpfield.value = "";
	
	var sitefield = document.getElementById("site");
	sitefield.value = "";
}

function removeException(thirdparty, site)
{
	backgroundPage.removeException(thirdparty, site);
	populateExceptionsList();
}

function removeAllExceptions()
{
	backgroundPage.removeAllExceptions();
	populateExceptionsList();
}

function removeAllDisabledSites()
{
    backgroundPage.removeAllDisabledSites();
    populateDisabledSitesAndPages();
}

function removeAllDisabledPages()
{
    backgroundPage.removeAllDisabledPages();
    populateDisabledSitesAndPages();
}

function getTableRow(vals, isHeader, type)
{
	var tr = document.createElement("tr");
	var len = vals.length;
	for (var i = 0; i < len; i++)
	{
		var td = document.createElement("td");
		if (i > 0)
		{
			td.setAttribute("align", "center");
		}
		if (isHeader)
		{
			var b = document.createElement("b");
			b.innerText = vals[i];
			td.appendChild(b);
		}
		else
		{
			if (i == len -1)
			{
				var button = document.createElement("button");
				button.innerText = vals[i];
				if (type == "tpexception")
				{
				    button.addEventListener("click", function() { removeException(vals[0], vals[1]); }, false);
				}
				else if (type == "disabledsite")
				{
				    button.addEventListener("click", function() { removeDisabledSite(vals[0]); }, false);				    
				}
				else if (type == "disabledpage")
				{
				    button.addEventListener("click", function() { removeDisabledPage(vals[0]); }, false);				    
				}
				td.appendChild(button);
			}
			else
			{
				td.innerText = vals[i];
			}
		}
		
		tr.appendChild(td);
	}

	return tr;
}

function initButtons()
{
	var buttonSave = document.getElementById("addException");
	buttonSave.addEventListener("click", function() { addException(); }, false);
	
	var buttonClear = document.getElementById("clearFields");
	buttonClear.addEventListener("click", function() { clearFields(); }, false);
	
	var buttonRemoveAll = document.getElementById("removeAllExceptions");
	buttonRemoveAll.addEventListener("click", function() { removeAllExceptions(); }, false);

	var buttonSave = document.getElementById("addDisabledSite");
	buttonSave.addEventListener("click", function() { addDisabledSite(); }, false);

	var buttonRemoveAll = document.getElementById("removeAllDisabledSites");
	buttonRemoveAll.addEventListener("click", function() { removeAllDisabledSites(); }, false);

	var buttonSave = document.getElementById("addDisabledPage");
	buttonSave.addEventListener("click", function() { addDisabledPage(); }, false);

	var buttonRemoveAll = document.getElementById("removeAllDisabledPages");
	buttonRemoveAll.addEventListener("click", function() { removeAllDisabledPages(); }, false);
}

function populateExceptionsList()
{
	var ael = backgroundPage.getAllowedExceptions();
	
	var divExceptions = document.getElementById("exceptions_list");
	
	while (divExceptions.firstChild)
	{
		divExceptions.removeChild(divExceptions.firstChild);
	}

	var tExceptions = window.document.createElement("table");
	tExceptions.setAttribute("border", "1");
	var hExceptions = getTableRow(["Third party", "Allowed site(s)", "Remove"], true, "tpexception");
	tExceptions.appendChild(hExceptions);
	
	var aelist = [];
	for (var ae in ael)
	{
		aelist.push([ae, ael[ae]]);
	}
	aelist.sort(function (a, b)
		{
			return a[0].localeCompare(b[0]);
		});
	
	for (var i = 0; i < aelist.length; i++)
	{
		var ae = aelist[i];
		for (var j = 0; j < ae[1].length; j++)
		{
			var tr = getTableRow([ae[0], ae[1][j], "Remove"], false, "tpexception");
			tExceptions.appendChild(tr);
		}
	}
	
	divExceptions.appendChild(tExceptions);
}

function populateDisabledSitesAndPages()
{
    var sitesList = backgroundPage.getDisabledSites();
    var pagesList = backgroundPage.getDisabledPages();
    
    var divSites = document.getElementById("disabledSites_list");
    
    while (divSites.firstChild)
    {
        divSites.removeChild(divSites.firstChild);
    }
    
    var tSites = window.document.createElement("table");
    tSites.setAttribute("border", "1");
    var hSites = getTableRow(["Priv3+ disabled at site", "Remove"], true, "disabledsite");
    tSites.appendChild(hSites);
    
    sitesList.sort(function (a, b)
        {
            return a.localeCompare(b);
        });
    for (var i = 0; i < sitesList.length; i++)
    {
        var tr = getTableRow([sitesList[i], "Remove"], false, "disabledsite");
        tSites.appendChild(tr);
    }
    
    divSites.appendChild(tSites);
    
    var divPages = document.getElementById("disabledPages_list");
    
    while (divPages.firstChild)
    {
        divPages.removeChild(divPages.firstChild);
    }
    
    var tPages = window.document.createElement("table");
    tPages.setAttribute("border", "1");
    var hPages = getTableRow(["Priv3+ disabled on page (URL)", "Remove"], true, "disabledpage");
    tPages.appendChild(hPages);
    
    pagesList.sort(function (a, b)
        {
            return a.localeCompare(b);
        });
    for (var i = 0; i < pagesList.length; i++)
    {
        var tr = getTableRow([pagesList[i], "Remove"], false, "disabledpage");
        tPages.appendChild(tr);
    }
    
    divPages.appendChild(tPages);
    
}

window.addEventListener("load", function() 
	{
		initButtons();
		populateExceptionsList();
		populateDisabledSitesAndPages();
	}, false);
