var backgroundPage = chrome.extension.getBackgroundPage();

function saveHighlightOptions(reset)
{
	var shouldHighlight;
	var highlightOptionsList = {};
	var inputElements = document.getElementsByTagName("input");
	var len = inputElements.length;
	for (var i = 0; i < len; i++)
	{
		var el = inputElements[i];
		if (el.getAttribute("type") == "checkbox")
		{
			var id = el.getAttribute("id");
			
			if (reset)
			{
				if (id == "shouldHighlight")
				{
					shouldHighlight = true;
				}
				else
				{
					highlightOptionsList[id] = true;
				}
			}
			else
			{
				var checked = el.getAttribute("checked");
				if (checked == "checked")
				{
					if (id == "shouldHighlight")
					{
						shouldHighlight = true;
					}
					else
					{
						highlightOptionsList[id] = true;
					}
				}
				else
				{
					if (id == "shouldHighlight")
					{
						shouldHighlight = false;
					}
					else
					{
						highlightOptionsList[id] = false;
					}
				}
			}
		}
	}
	backgroundPage.setHighlightOptions(shouldHighlight, highlightOptionsList);
	populateHighlightOptions(shouldHighlight, highlightOptionsList);
}

function toggleEnabledCheckboxes(event)
{
	var inputElements = document.getElementsByTagName("input");
	var len = inputElements.length;
	for (var i = 0; i < len; i++)
	{
		var el = inputElements[i];
		if (el.getAttribute("type") == "checkbox")
		{
			var id = el.getAttribute("id");
			if (id != "shouldHighlight")
			{
				if (el.getAttribute("disabled"))
				{
					el.removeAttribute("disabled");
				}
				else
				{
					el.setAttribute("disabled", true);
				}
			}
		}
	}
}

function toggleCheckbox(event)
{
	var checkbox = event.target;
	var checked = checkbox.getAttribute("checked");
	if (checked == "checked")
	{
		checkbox.removeAttribute("checked");
	}
	else
	{
		checkbox.setAttribute("checked", "checked");
	}
	
	var id = checkbox.getAttribute("id");
	if (id == "shouldHighlight")
	{
		toggleEnabledCheckboxes(event);
	}
}

function getCheckboxElement(id, checked, shouldHighlight)
{
	var checkbox = document.createElement("input");
	checkbox.setAttribute("type", "checkbox");
	checkbox.setAttribute("id", id);
	checkbox.setAttribute("name", id);
	checkbox.setAttribute("value", id);
	
	if (checked)
	{
		checkbox.setAttribute("checked", "checked");
	}
	
	if (!shouldHighlight)
	{
		checkbox.setAttribute("disabled", true);
	}
	
	checkbox.addEventListener('click', function(event) { toggleCheckbox(event); } );
	
	return checkbox;
}

function getHighlightElement(id, checked, text, shouldHighlight)
{
	var tr = document.createElement("tr");
	
	var td = document.createElement("td");
	var checkbox = getCheckboxElement(id, checked, shouldHighlight);
	td.appendChild(checkbox);
	tr.appendChild(td);
	
	var td2 = document.createElement("td");
	var label = document.createElement("label");
	var text2 = document.createTextNode(text);
	label.appendChild(text2);
	td2.appendChild(label);
	tr.appendChild(td2);
	
	return tr;
}

function populateHighlightOptions(shouldHighlight, highlightOptionsList)
{
	var divOptions = document.getElementById("options");

	while (divOptions.firstChild)
	{
		divOptions.removeChild(divOptions.firstChild);
	}
	
	var table = document.createElement("table");
	// XXX: always enabled
	var trShouldHighlight = getHighlightElement("shouldHighlight", shouldHighlight, "Highlight third party content", true);
	table.appendChild(trShouldHighlight);

	var hr = document.createElement("hr");
	table.appendChild(hr);

	var labels = {};
	labels["main"] = "Elements in main document (red)";
	labels["iframe"] = "Elements in iframes (orange)";
	labels["other"] = "Potential third party elements (yellow)";
	labels["clicked"] = "Clicked elements (purple)";
	labels["reloaded"] = "Reloaded (with cookies) elements (turquoise)";
	labels["excepted"] = "Excepted elements (green)";

	for (var t in highlightOptionsList)
	{
		var trOption = getHighlightElement(t, highlightOptionsList[t], labels[t], shouldHighlight);
		table.appendChild(trOption);
	}

	var table2 = document.createElement("table");
	var tr = document.createElement("tr");
	
	var tdSave = document.createElement("td");
	var button = document.createElement("input");
	button.setAttribute("id", "button_highlight_save");
	button.setAttribute("type", "button");
	button.setAttribute("value", "Save");

	button.addEventListener('click', function() { saveHighlightOptions(false); } );
	
	tdSave.appendChild(button);
	tr.appendChild(tdSave);
	
	var tdReset = document.createElement("td");
	var button2 = document.createElement("input");
	button2.setAttribute("id", "button_highlight_reset");
	button2.setAttribute("type", "button");
	button2.setAttribute("value", "Reset to default");
	
	button2.addEventListener('click', function() {saveHighlightOptions(true); } );
	
	tdReset.appendChild(button2);
	tr.appendChild(tdReset);
	
	table2.appendChild(tr);
	
	divOptions.appendChild(table);
	divOptions.appendChild(table2);
	
}

function updateWindow()
{
    // XXX: get all options
    // 1. should highlight and highlight options
    var highlightOptions = backgroundPage.getHighlightOptions();
    var shouldHighlight = highlightOptions[0];
    var highlightOptionsList = highlightOptions[1];
    populateHighlightOptions(shouldHighlight, highlightOptionsList);

}

window.addEventListener("DOMContentLoaded", function(event)
	{
		updateWindow();
	});
